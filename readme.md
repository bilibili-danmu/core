![Bilibili-Danmu/Core](https://gitlab.com/bilibili-danmu/core/raw/master/logo.png)

> bilibili-danmu是一款使用bash(shell)语言编写的哔哩哔哩弹幕下载转换工具

## 简介

顾名思义，bilibili-danmu是一款下载和管理哔哩哔哩弹幕的工具。使用纯shell开发，迭代速度较快。

## 功能/特性

-  下载哔哩哔哩视频/番剧弹幕

-  通过视频cid/番剧ss获取弹幕

-  生成人类可读的avid列表(视频列表)

-  批量导入番剧或视频(分区)

-  (批量)导出弹幕(通过github@m13252开发的danmaku2ass项目)

## 快速开始

### 1	基本配置

- 网络连接(当然了)
- 充足的磁盘空间(下载所有番剧弹幕约占4.0GiB，~~开启git后约5.5GiB~~ git已经不推荐使用，请等待tar增量打包功能的实现)
- CPU(jq解析json时会用到，如果速度慢的话会严重拖慢速度，尤其是一键解析视频cid时)(可以通过调整线程数量降低负荷，但是速度也会降低)

### 2	环境要求

务必安装'jq'软件包。如果需要弹幕转换导出功能请安装'python'。

Debian系用户:

`# apt install jq`

Arch系用户:

`# pacman -S jq`

其他发行版用户请自行安装。

### 3	运行

bilibili-danmu是一个组件化的程序。下载最新的脚本可以直接从master分支克隆。下载完毕后执行`bash ./danmu.sh`即可。

`git clone https://gitlab.com/bilibili-danmu/core.git

## ~~高级(批量)操作~~

~~执行下列操作会消耗大量的CPU和磁盘空间，请三思而后行。~~

### ~~写入目标视频名称，下载弹幕~~

~~直接运行`bash ./danmu.sh`即可。~~

### ~~弹幕批量下载~~

- ~~执行`bash ./fanjucid.sh`就可以一键将所有番剧cid存入list中。~~
- ~~执行`bash ./videocid.sh`就可以一键将所有视频cid存入list中。~~

**所有操作目前均可使用danmu.sh操作。**

## 使用Jupyter Notebook

`Bilibili_Danmu.ipynb`是本工具的官方笔记本。用户可以下载并执行此笔记本。用户还可以使用Google Colab运行，相当于使用免费的CPU和网络进行计算。`

## FAQ

### 1.程序报错怎么办?

​	通常的解决办法:删除所有数据，`git pull`获取最新版本。请注意，bilibili-danmu处于开发状态--程序可能出现各种意想不到的错误。

### 2.我发现了一个bug/可以改进的地方，怎么提交?

- ​	如果您知道该如何改正，请提交一个[merge request](https://gitlab.com/bilibili-danmu/core/merge_requests)。
- ​	如果您不知道该如何改正，请提交一个[issue](https://gitlab.com/bilibili-danmu/core/issues)。

> 请务必在issue里面提交:
>
> ​	1.您的发行版及版本信息;
>
> ​	2.程序的所有输出;
>
> ​	3.(可选)bug发生后的所有残留文件(请将整个程序文件夹用tar打包并上传).

## TODO
-  完成Tar打包功能(增量打包)

-  内容分发网络

-  更新模块

## 许可证

> Bilibili-danmu使用GNU General Public License v3.0协议开源。

> Danmaku2ass由github@m13252开发，根据GPL v3分发。
