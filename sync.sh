sync_version="0.3.2"
sync_description="弹幕下载高级线程组件"
startsync(){
    thread_cur=0
    cat cid.list | while read line
    do
        if [ "${thread_cur}" == "${thread}" ]
        then
            wait
            thread_cur=0
        fi
        line_1="${line:0:1}"
        if [ "${line_1}" == "-" ]
        then
            title="${line:3:64}"
        else
            line_2="${line:2:1}"
            if [ "${line_2}" == "标" ]
            then
                title_raw="${line:5:32}"
                title_raw2="${title_raw/\"/}"
                title_raw3="${title_raw2/\//_}"
                title="${title}_${title_raw3}"
                unset title_raw2
                unset title_raw
                unset title_raw3
            else
                if [ "${line_2}" != "分" ]
                then
                    line_3="${line:4:1}"
                    if [ "${line_3}" != ":" ]
                    then
                        thread_cur=`expr ${thread_cur} + 1`
                        line_real=${line:2:32}
                        if [ "${title}" == "" ]
                        then
                            title="Unknown"
                        fi
                        echo "|-${title} ${line_real}任务被提交至线程${thread_cur}"
                        danmu_sync_thread ${danmu_source} ${thread_cur} ${line_real} ${danmu_folder} ${title} &
                    else
                        title_raw="${line:5:64}"
                        title_raw2="${title_raw/\"/}"
                        title_raw3="${title_raw2/\//_}"
                        title="${title}_${title_raw3}"
                        unset title_raw2
                        unset title_raw
                        unset title_raw3
                    fi
                fi
            fi
        fi
    done
    unset thread_cur
    wait
    sleep 2s
    if [ "${tar_enable}" == "true" ]
    then
        echo "Backing up..."
        if [ ! -f ${tar_folder}/tarinfo ]
        then
            tar -g ${tar_folder}/tarinfo -czf ${tar_folder}/backup-full.tar.gz ${danmu_folder}/
            echo "1" >${tar_folder}/next.count
        else
            current_count=$(cat ${tar_folder}/next.count)
            tar -g ${tar_folder}/tarinfo -czf ${tar_folder}/backup-incre${current_count}.tar.gz ${danmu_folder}/
            current_count=`expr ${current_count} + 1`
            echo "${current_count}" >${tar_folder}/next.count
            unset current_count
        fi
    fi
    main_loop
}