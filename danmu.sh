#!/bin/bash
danmu_version="0.3.2"
danmu_description="哔哩哔哩弹幕下载工具核心程序"
core_about(){
    clear
    echo "---------------------------"
    core_ascii_art_3d
    echo -e "\033[47;30m哔哩哔哩弹幕下载工具\033[0m"
    echo -e "By gitlab@void_life"
    echo -e "本程序及源码根据GPL-3.0-or-later协议共享。"
    if [ -f danmaku2ass.py ]
    then
        echo -e "Danmaku2ASS是由github@m13253开发的弹幕格式转换软件，根据GPL3分发。"
    fi
    echo "---------------------------"
    echo -e "\033[35;37m核心程序版本:\033[0m"
    bangumititle_sha1=$(core_sha1 danmu_title.sh)
    echo -e "\033[35;37m${bangumititle_description} ${bangumititle_version}.${bangumititle_sha1}\033[0m"
    core_function_sha1=$(core_sha1 core_function.sh)
    echo -e "\033[35;37m${core_function_description} ${core_function_version}.${core_function_sha1}\033[0m"
    ascii_sha1=$(core_sha1 ascii_art.sh)
    echo -e "\033[35;37m${ascii_description} ${ascii_version}.${ascii_sha1}\033[0m"
    hook_sha1=$(core_sha1 hook.sh)
    echo -e "\033[35;37m${hook_description} ${hook_version}.${hook_sha1}\033[0m"
    danmu_sha1=$(core_sha1 danmu.sh)
    echo -e "\033[35;37m${danmu_description}  ${danmu_version}.${danmu_sha1}\033[0m"
    danmu_setting_sha1=$(core_sha1 danmu_setting_function.sh)
    echo -e "\033[35;37m${danmu_setting_description}  ${danmu_setting_version}.${danmu_setting_sha1}\033[0m"
    danmu_core_sha1=$(core_sha1 danmu_function.sh)
    echo -e "\033[35;37m${danmu_core_description} ${danmu_core_version}.${danmu_core_sha1}\033[0m"
    fanjucid_sha1=$(core_sha1 fanjucid.sh)
    echo -e "\033[35;37m${fanjucid_description} ${fanjucid_version}.${fanjucid_sha1}\033[0m"
    sync_thread_sha1=$(core_sha1 sync_thread.sh)
    echo -e "\033[35;37m${sync_thread_description} ${sync_thread_version}.${sync_thread_sha1}\033[0m"
    sync_sha1=$(core_sha1 sync.sh)
    echo -e "\033[35;37m${sync_description} ${sync_version}.${sync_sha1}\033[0m"
    video_cid_analysis_thread_sha1=$(core_sha1 video_cid_analysis_thread.sh)
    echo -e "\033[35;37m${video_cid_analysis_thread_description} ${video_cid_analysis_thread_version}.${video_cid_analysis_thread_sha1}\033[0m"
    video_cid_download_thread_sha1=$(core_sha1 video_cid_download_thread.sh)
    echo -e "\033[35;37m${video_cid_download_thread_description} ${video_cid_download_thread_version}.${video_cid_download_thread_sha1}\033[0m"
    videocid_sha1=$(core_sha1 videocid.sh)
    echo -e "\033[35;37m${videocid_description} ${videocid_version}.${videocid_sha1}\033[0m"
    multisync_sha1=$(core_sha1 multisync.sh)
    echo -e "\033[35;37m${multisync_description} ${multisync_version}.${multisync_sha1}\033[0m"
    export_sha1=$(core_sha1 export.sh)
    echo -e "\033[35;37m${export_description} ${export_version}.${export_sha1}\033[0m"
    echo "---------------------------"
    echo "单击回车返回主界面..."
    read NOTHING
    main_loop
}
main_loop(){
    ua_determine
    clear
    echo "---------------------------"
    echo "哔哩哔哩弹幕下载工具 ver ${danmu_version}"
    core_ascii_art
    echo "---------------------------"
    echo -e "\033[31m1\033[0m--\033[47;30m写入视频清单\033[0m"
    echo -e "\033[31m2\033[0m--\033[47;30m查看所有已记录的视频条目\033[0m"
    echo -e "\033[31m3\033[0m--\033[47;30m开始依据清单下载(更新)弹幕\033[0m"
    echo -e "\033[31m4\033[0m--\033[47;30m批量导入条目(导入所有番剧\\导入指定分区条目)\033[0m"
    echo -e "\033[31m5\033[0m--\033[47;30m转换并导出弹幕\033[0m"
    echo -e "\033[31m6\033[0m--\033[47;30m工具设置\033[0m"
    echo -e "\033[31m9\033[0m--\033[47;30m关于bilibili-danmu\033[0m"
    echo -e "\033[31m其他键\033[0m--\033[47;30m退出\033[0m"
    echo "---------------------------"
    read -p "请选择:" switch
    case $switch in
    1)  writedata
    ;;
    2)  viewdata
    ;;
    3)  startsync
    ;;
    4)  multi_sync
    ;;
    5)  export_danmu_ui
    ;;
    6)  setting_ui
    ;;
    9)  core_about
    ;;
    *)  core_exit
    ;;
    esac
}
source core_function.sh
if [ ! -f setting.cfg ]
then
    new_setting
fi
source setting.cfg
if [ "${danmu_debug}" == "isdebug" ]
then
    source hook.sh
    debug_init
else
    hook_version="未启用"
    hook_description="bash debug钩子"
fi
source danmu_setting_function.sh
source danmu_function.sh
source ascii_art.sh
source danmu_title.sh
source fanjucid.sh
source sync_thread.sh
source sync.sh
source video_cid_analysis_thread.sh
source video_cid_download_thread.sh
source videocid.sh
source multisync.sh
source export.sh
determine_jq_and_python
main_loop
