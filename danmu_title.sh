bangumititle_version="1.0"
bangumititle_description="番剧标题获取组件"
#一个非常奇怪而且繁琐的流程，但是应该不会变化了(
get_bangumi_title(){
    target_bangumi=${1}
    real_url="https://www.bilibili.com/bangumi/play/ss${target_bangumi}"
    curl "${real_url}" -s -S --compressed -o danmu.tmp
    unset real_url
    cat danmu.tmp | while read line
    do
        verify="${line##*property??og?title??content=*meta?property??og?url??content?*}"
        if [ "${verify}" == "" ]
        then
            got1="${line##*property??og?title??content=?}"
            got2="${got1%%???meta?property??og?type??content??video?anime???meta?property??og?url??content?*}"
            echo ${got2} >title.tmp
        fi
    done
    rm danmu.tmp
    av_title=$(cat title.tmp)
    rm title.tmp
    if [ "${av_title}" == "" ]
    then
        #如果失败则使用备用方案(获取第一集)
        url="https://bangumi.bilibili.com/web_api/get_ep_list?season_id=${target_bangumi}&season_type=1"
        curl ${url} -s -H "${ua_true}" --compressed >gettitle.tmp
        cat gettitle.tmp|jq ".result[0].avid">getavid.gettitle.result.tmp
        rm getcid.tmp
        firstavid=$(cat getavid.gettitle.result.tmp)
        rm getavid.gettitle.result.tmp
        url="https://api.bilibili.com/x/web-interface/view?aid=${firstavid}"
        unset firstavid
        curl ${url} -s -H "${ua_true}" --compressed >getcid.tmp
        cat getcid.tmp|jq '.data.title'>gettitle.result.tmp
        rm getcid.tmp
        av_title_raw=$(cat gettitle.result.tmp)
        #稍微优化一下
        av_title=${av_title_raw%[0-9][0-9]*}
        unset av_title_raw
        rm gettitle.result.tmp
    fi
}