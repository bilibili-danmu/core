danmu_core_version="0.3.2"
danmu_core_description="哔哩哔哩弹幕下载工具核心组件"
writedata(){
    clear
    echo "-----------------------"
    echo "      写入视频清单"
    echo "-----------------------"
    echo -e "\033[31m1\033[0m--\033[47;30m写入番剧条目\033[0m"
    echo -e "\033[31m2\033[0m--\033[47;30m写入普通视频条目\033[0m"
    echo -e "\033[31m其他键\033[0m--\033[47;30m返回\033[0m"
    echo -e "-----------------------"
    read -p "请选择:" switch
    case $switch in
    1)  writedata_fanju
    ;;
    2)  writedata_regular
    ;;
    *)  main_loop
    ;;
    esac
}
writedata_regular(){
    clear
    echo "---------------------------"
    echo "      写入普通视频条目"
    echo "---------------------------"
    read -p "视频av号:" aid
    url="https://api.bilibili.com/x/web-interface/view?aid=${aid}"
    curl ${url} -H "${ua_true}" --compressed -o gettitle_cid.tmp
    url="https://api.bilibili.com/x/player/pagelist?aid=${aid}&jsonp=jsonp"
    curl ${url} -H "${ua_true}" --compressed -o getcid.tmp
    cat getcid.tmp|jq '.data[] | .part, .cid'>getcid.result.tmp
    cat gettitle_cid.tmp|jq '.data.title'>gettitle.result.tmp
    rm getcid.tmp
    rm gettitle_cid.tmp
    av_title=$(cat gettitle.result.tmp)
    rm gettitle.result.tmp
    if [ "${av_title}" == "" ]
    then
        av_title="无标题"
    fi
    echo "-视频${aid}" >>cid.list
    echo " |-标题:${av_title}" >>cid.list
    unset aid
    unset av_title
    echo " |-分集" >>cid.list
    cid_count=0
    cat getcid.result.tmp | while read line_2
    do
        if [ ${line_2:0:1} != "\"" ]
        then
            cid_count=`expr ${cid_count} + 1`
            echo "  |-${line_2}" >> cid.list
        else
            #有时标题为空
            if [ "${line_2}" == "\"\"" ]
            then
                #line_2="\"无题\""
                line_2="\"${av_title}\""
            fi
            echo "  |-题目:${line_2}" >> cid.list
        fi
        echo "${cid_count}" >cid.count
    done
    rm getcid.result.tmp
    cid_count=$(cat cid.count)
    rm cid.count
    echo "成功添加${cid_count}个视频。"
    unset cid_count
    echo "按回车返回\"写入视频清单\""
    read NOTHING
    writedata
}
writedata_fanju(){
    clear
    echo "---------------------------"
    echo "         写入番剧条目"
    echo "---------------------------"
    read -p "番剧sp号:" spid
    url="https://bangumi.bilibili.com/web_api/get_ep_list?season_id=${spid}&season_type=1"
    curl ${url} -H "${ua_true}" --compressed -o getcid.tmp
    cat getcid.tmp|jq '.result[].cid'>getcid.result.tmp
    get_bangumi_title ${spid}
    #番剧不知为何标题没有引号
    echo "-番剧\"${spid}\"" >>cid.list
    echo " |-标题:${av_title}" >>cid.list
    echo " |-分集" >>cid.list
    cid_count=0
    for line in $(cat getcid.result.tmp)
    do
        cid_count=`expr ${cid_count} + 1`
        echo "  |-${line}" >> cid.list
    done
    rm getcid.result.tmp
    echo "成功添加${cid_count}个番剧分集。"
    unset cid_count
    unset spid
    unset firstavid
    unset av_title
    echo "按回车返回\"写入视频清单\""
    read NOTHING
    writedata
}
viewdata(){
    clear
    echo "---------------------------"
    echo "         cid记录"
    echo "---------------------------"
    if [ -f cid.list ]
    then
        cat cid.list
    else
        echo "无cid数据。"
    fi
    echo "按回车返回\"主页\""
    read NOTHING
    main_loop
}