#!/bin/bash
video_cid_analysis_thread_version="0.3.2"
video_cid_analysis_thread_description="普通视频批量高级线程组件"
cidmuld_video_optimize(){
    current_page=${1}
    ps=${2}
    thread_code=${3}
    max_download_thread=${4}
    real_url="https://api.bilibili.com/archive_rank/getarchiverankbypartion?jsonp=jsonp&tid=${ps}&pn=${current_page}"
    DATE=$(date +%H:%M:%S)
    echo " |-${thread_code} 处理第${current_page}页"
    curl ${real_url} -H "Referer: https://m.bilibili.com/channel/22.html" -H 'User-Agent: Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Mobile Safari/537.36' --compressed -s -o getcid_${thread_code}.tmp
    unset real_url
    cat getcid_${thread_code}.tmp|jq '.data.archives[] | .title, .aid'>getcid_${thread_code}.result.tmp
    rm getcid_${thread_code}.tmp
    download_thread=0
    cat getcid_${thread_code}.result.tmp | while read line
    do
        if [ ${download_thread} == ${max_download_thread} ]
        then
            wait
            if [ ! -f cid_${thread_code}.list ]
            then
                touch cid_${thread_code}.list
            fi
            thread_done=0
            done_thread=0
            while(( ${thread_done} != 1 ))
            do
                done_thread=`expr ${done_thread} + 1`
                if [ ${done_thread} == ${max_download_thread} ]
                then
                    thread_done=1
                fi
                if [ -f cid_${thread_code}_${done_thread}.list ]
                then
                    cat cid_${thread_code}_${done_thread}.list >>cid_${thread_code}.list
                    rm cid_${thread_code}_${done_thread}.list
                fi
            done
            unset thread_done
            unset done_thread
            download_thread=0
        fi
        if [ ${line:0:1} != "\"" ]
        then
            #需要处理的行
            if [ "${last_title}" == "" ]
            then
                last_title="Unknown"
            fi
            if [ -f cid.list ]
            then
                grep "视频${line}" cid.list >/dev/null
                if [ $? -eq 0 ]; then
                    DATE=$(date +%H:%M:%S)
                    echo " |-${thread_code} cid列表中存在${last_title} (cid:${line})，不再更新。"
                else
                    download_thread=`expr ${download_thread} + 1`
                    cidmuld_video_basic ${download_thread} ${line} ${thread_code} ${last_title} &
                fi
            else
                download_thread=`expr ${download_thread} + 1`
                cidmuld_video_basic ${download_thread} ${line} ${thread_code} ${last_title} &
            fi
        else
            if [ "${line}" == "" ]
            then
                last_title="Unknown"
            else
                last_title=${line}
            fi
        fi
    done
    sleep 1
    wait
    page_done=0
    done_thread=0
    while(( ${page_done} != 1 ))
    do
        if [ ${done_thread} == ${max_download_thread} ]
        then
            page_done=1
        else
            done_thread=`expr ${done_thread} + 1`
            if [ -f cid_${thread_code}_${done_thread}.list ]
            then
                cat cid_${thread_code}_${done_thread}.list >>cid_${thread_code}.list
                rm cid_${thread_code}_${done_thread}.list
            fi
        fi
    done
    unset page_done
    unset done_thread
    download_thread=0
    unset download_thread
    rm getcid_${thread_code}.result.tmp
}