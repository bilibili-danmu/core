danmu_setting_version="0.4.2"
danmu_setting_description="哔哩哔哩弹幕下载核心设置组件"
setting_ui(){
    clear
    echo "---------------------------"
    echo "           设置"
    echo "---------------------------"
    echo -e "\033[31m1\033[0m--\033[47;30m更改弹幕保存文件夹\033[0m"
    echo -e "\033[31m2\033[0m--\033[47;30m更改程序UA\033[0m"
    echo -e "\033[31m3\033[0m--\033[47;30m切换弹幕下载源\033[0m"
    echo -e "\033[31m4\033[0m--\033[47;30m设置多线程下载\033[0m"
    echo -e "\033[31m5\033[0m--\033[47;30m设置下载(更新)模式\033[0m"
    echo -e "\033[31m6\033[0m--\033[47;30m修改视频cid导入线程\033[0m"
    if [ -f danmaku2ass.py ]
    then
        echo -e "\033[31m7\033[0m--\033[47;30m禁用弹幕导出功能\033[0m"
    else
        echo -e "\033[31m7\033[0m--\033[47;30m启用弹幕导出功能\033[0m"
    fi
    if [ "${danmu_debug}" == "isdebug" ]
    then
        echo -e "\033[31m8\033[0m--\033[47;30m禁用调试模式\033[0m"
    else
        echo -e "\033[31m8\033[0m--\033[47;30m启用调试模式\033[0m"
    fi
    if [ -f danmaku2ass.py ]
    then
        echo -e "\033[31m9\033[0m--\033[47;30m调整弹幕导出分辨率\033[0m"
    fi
    echo -e "\033[31m0\033[0m--\033[47;30m恢复初始设置\033[0m"
    echo -e "\033[31m其他键\033[0m--\033[47;30m返回\033[0m"
    echo "---------------------------"
    read -p "请选择:" switch
    case $switch in
    1)  setting_danmu_folder
        ;;
    2)  setting_ua
        ;;
    3)  setting_danmu_source
        ;;
    4)  setting_multi_thread
        ;;
    5)  setting_download_mode
        ;;
    6)  setting_video_multi
        ;;
    7)  change_export_setting
        ;;
    8)  change_debug_setting
        ;;
    9)  change_res
        ;;
    0)  reset_setting
        ;;
    *)  main_loop
        ;;
    esac
}
setting_danmu_folder(){
    clear
    echo "---------------------------"
    echo "        弹幕文件夹设置"
    echo "---------------------------"
    echo "目前文件夹:${danmu_folder}"
    danmu_folder_old=${danmu_folder}
    read -p "请输入文件夹名称(留空不保存):" danmu_folder
    if [ "${danmu_folder}" == "" ]
    then
        danmu_folder=${danmu_folder_old}
        unset danmu_folder_old
        setting_ui
    fi
    if [ ! -d ${danmu_folder} ]
    then
        mkdir ${danmu_folder}
    fi
    if [ ! -d ${danmu_folder} ]
    then
        echo "错误:无法找到${danmu_folder}，同时本脚本在创建该目录时出现错误。将不会保存配置。"
        danmu_folder=${danmu_folder_old}
        unset danmu_folder_old
    fi
    mkdir tmp
    cd tmp
    if [ -d ${danmu_folder} ]
    then
        echo "您输入了绝对路径，为了保证兼容性我们会创建一个软链接指向文件夹。"
        cd ..
        ln -i -s ${danmu_folder} danmu_folder
        danmu_folder="danmu_folder"
    else
        cd ..
    fi
    rm -r tmp
    unset danmu_folder_old
    rewrite_setting
}
setting_ua(){
    clear
    echo "---------------------------"
    echo "        更改程序UA"
    echo "---------------------------"
    echo "-可用的UA:"
    echo "|-chrome-xp"
    echo " |->User-Agent: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36"
    echo "|-netscape-xp"
    echo " |->User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.12) Gecko/20080219 Firefox/2.0.0.12 Navigator/9.0.0.6"
    echo "|-firefox-vista"
    echo " |->User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7"
    echo "|-firefox-7"
    echo " |->User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1"
    echo "|-firefox-android"
    echo " |->User-Agent: Mozilla/5.0 (Android; Mobile; rv:14.0) Gecko/14.0 Firefox/14.0"
    echo "|-opera-mac"
    echo " |->User-Agent: Opera/9.80 (Macintosh; Intel Mac OS X; U; en) Presto/2.2.15 Version/10.00"
    echo "|-safari-iphone"
    echo " |->User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10"
    echo "|-ie-8"
    echo " |->User-Agent: Mozilla/4.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/4.0; SLCC1)"
    echo "|-360speed-xp"
    echo " |->User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ;  QIHU 360EE)"
    echo "|-safari-ipad"
    echo " |->User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10"
    echo "|-webkit-android"
    echo " |->User-Agent: Mozilla/5.0 (Linux; U; Android 4.0.3; zh-cn; M032 Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"
    echo "|-firefox-linux"
    echo " |->User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0"
    echo "|-chrome-linux"
    echo " |->User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36"
    read -p "请选择UA:(例:输入\"chrome-linux\")" ua
    rewrite_setting
}
setting_danmu_source(){
    clear
    echo "---------------------------"
    echo "        弹幕下载源调整"
    echo "---------------------------"
    echo -e "\033[31msmart\033[0m--\033[47;30m智能模式\033[0m"
    echo -e "\033[31mapi\033[0m--\033[47;30m使用bilibili官方api\033[0m"
    echo -e "\033[31mcommit\033[0m--\033[47;30m使用bilibili自己使用的地址\033[0m"
    echo -e "\033[47;30m建议选择smart模式。\033[0m"
    read -p "请选择:" danmu_source
    rewrite_setting
}
setting_multi_thread(){
    clear
    echo "---------------------------"
    echo "        线程数量调整"
    echo "---------------------------"
    echo "目前线程数量:${thread}"
    read -p "新的线程数量(留空不保存设置):" danmu_d_thread
    if [ "${danmu_d_thread}" == "" ]
    then
        unset danmu_d_thread
        setting_ui
    else
        thread=${danmu_d_thread}
        unset danmu_d_thread
        rewrite_setting
    fi
}
setting_download_mode(){
    clear
    echo "---------------------------"
    echo "        下载(更新)模式"
    echo "---------------------------"
    echo -e "\033[31m更新模式\033[0m:\033[47;30m不管以前下没下载过该视频弹幕，程序均会下载。通过git记录或tar增量打包，适合大部分弹幕都下载完毕后选择。\033[0m"
    echo -e "\033[31m批量下载模式\033[0m:\033[47;30m如果弹幕文件夹中存在某视频弹幕则不下载。\033[0m"
    if [ ${download_mode} == 1 ]
    then
        echo "目前下载模式:更新模式"
    else
        echo "目前下载模式:批量下载模式"
    fi
    echo "---------------------------"
    download_mode_old=${download_mode}
    read -p "新的下载模式:(1--更新模式, 2--批量下载模式，其他不保存)" download_mode
    if [ "${download_mode}" != "1" ]
    then
        if [ "${download_mode}" != "2" ]
        then
            download_mode=${download_mode_old}
            unset download_mode_old
            setting_ui
        fi
    fi
    rewrite_setting
}
reset_setting(){
    echo "danmu_folder=danmu" >setting.cfg
    echo "ua=chrome_linux" >>setting.cfg
    echo "danmu_source=smart" >>setting.cfg
    echo "bg=0" >>setting.cfg
    echo "danmu_d_thread=5" >>setting.cfg
    echo "download_mode=1" >>setting.cfg
    echo "max_analysis_thread=3" >>setting.cfg
    echo "max_download_thread=2" >>setting.cfg
    echo "danmu_debug=no" >>setting.cfg
    echo "res=1920x1080" >>setting.cfg
    if [ ! -d danmu ]
    then
        mkdir danmu
    fi
    echo "配置文件重置成功。"
    setting_ui
}
rewrite_setting(){
    echo "正在重新生成配置文件"
    echo "请勿现在关闭程序"
    echo "danmu_folder=${danmu_folder}" >setting.cfg
    echo "ua=${ua}" >>setting.cfg
    echo "danmu_source=${danmu_source}" >>setting.cfg
    echo "bg=${bg}" >>setting.cfg
    echo "thread=${thread}" >>setting.cfg
    echo "download_mode=${download_mode}" >>setting.cfg
    echo "max_analysis_thread=${max_analysis_thread}" >>setting.cfg
    echo "max_download_thread=${max_download_thread}" >>setting.cfg
    echo "danmu_debug=${danmu_debug}" >>setting.cfg
    echo "res=${res}" >>setting.cfg
    #调整ua_true值
    ua_determine
    setting_ui
}
ua_determine(){
    case $ua in
    chrome-xp)
        ua_true="User-Agent: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36"
        ;;
    netscape-xp)
        ua_true="User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.12) Gecko/20080219 Firefox/2.0.0.12 Navigator/9.0.0.6"
        ;;
    firefox-vista)
        ua_true="User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en; rv:1.9.1.7) Gecko/20091221 Firefox/3.5.7"
        ;;
    firefox-7)
        ua_true="User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64; rv:15.0) Gecko/20120427 Firefox/15.0a1"
        ;;
    firefox-android)
        ua_true="User-Agent: Mozilla/5.0 (Android; Mobile; rv:14.0) Gecko/14.0 Firefox/14.0"
        ;;
    opera-mac)
        ua_true="User-Agent: Opera/9.80 (Macintosh; Intel Mac OS X; U; en) Presto/2.2.15 Version/10.00"
        ;;
    safari-iphone)
        ua_true="User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10"
        ;;
    ie-8)
        ua_true="User-Agent: Mozilla/4.0 (compatible; MSIE 10.0; Windows NT 6.2; WOW64; Trident/4.0; SLCC1)"
        ;;
    360speed-xp)
        ua_true="User-Agent: Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ;  QIHU 360EE)"
        ;;
    safari-ipad)
        ua_true="User-Agent: Mozilla/5.0 (iPad; U; CPU OS 3_2 like Mac OS X; en-us) AppleWebKit/531.21.10 (KHTML, like Gecko) Version/4.0.4 Mobile/7B334b Safari/531.21.10"
        ;;
    webkit-android)
        ua_true="User-Agent: Mozilla/5.0 (Linux; U; Android 4.0.3; zh-cn; M032 Build/IML74K) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"
        ;;
    firefox-linux)
        ua_true="User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:69.0) Gecko/20100101 Firefox/69.0"
        ;;
    chrome-linux)
        ua_true="User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36"
        ;;
    esac
}
setting_video_multi(){
    clear
    echo "---------------------------"
    echo "     修改视频cid导入线程"
    echo "---------------------------"
    echo "当前配置:"
    echo -e "  \033[31m高级线程数量\033[0m:\033[47;30m${max_analysis_thread}\033[0m"
    echo -e "  \033[31m基本线程数量\033[0m:\033[47;30m${max_download_thread}\033[0m"
    echo "---------------------------"
    read -p "高级线程数量:" max_analysis_thread
    read -p "基本线程数量:" max_download_thread
    rewrite_setting
}
change_export_setting(){
    clear
    echo "---------------------------"
    if [ -f danmaku2ass.py ]
    then
        echo "弹幕导出功能目前处于启用状态。"
        echo "---------------------------"
        echo -e "\033[31mY\033[0m--\033[47;30m保持当前状态\033[0m"
        echo -e "\033[31mN\033[0m--\033[47;30m禁用弹幕导出功能\033[0m"
        echo "---------------------------"
    else
        if [ -f danmaku2ass.py.diabled ]
        then
            echo "弹幕导出功能目前处于禁用状态。"
            echo "---------------------------"
            echo -e "\033[31mY\033[0m--\033[47;30m启用弹幕导出功能\033[0m"
            echo -e "\033[31mN\033[0m--\033[47;30m保持当前状态\033[0m"
            echo "---------------------------"
        else
            echo -e "\033[31m弹幕导出功能外部组件未安装。\033[0m"
            echo -e "\033[31m如果想启动弹幕导出功能，请下载\"danmaku2ass.py\"文件。\033[0m"
            setting_ui
        fi
    fi
    read -p "请选择:" switch
    case ${switch} in
    y)  enable_export
    ;;
    Y)  enable_export
    ;;
    n)  disable_export
    ;;
    N)  disable_export
    ;;
    esac
    setting_ui
}
enable_export(){
    if [ -f danmaku2ass.py.diabled ]
    then
        mv danmaku2ass.py.diabled danmaku2ass.py
        echo "弹幕导出启用完成!"
    fi
}
disable_export(){
    if [ -f danmaku2ass.py ]
    then
        mv danmaku2ass.py danmaku2ass.py.diabled
        echo "弹幕导出禁用完成!"
    fi
}
change_debug_setting(){
    if [ "${danmu_debug}" == "isdebug" ]
    then
        danmu_debug="no"
    else
        danmu_debug="isdebug"
    fi
    echo "debug设置重启程序生效。"
    rewrite_setting
}
change_res(){
    clear
    echo "---------------------------"
    echo "     调整弹幕导出分辨率"
    echo "---------------------------"
    echo -e "\033[31m目前的分辨率\033[0m:\033[47;30m${res}\033[0m"
    read -p "新的分辨率:(以\'x\'分割，留空不调整)" res_change
    if [ "${res_change}" != "" ]
    then
        res=${res_change}
    fi
    unset res_change
    rewrite_setting
}