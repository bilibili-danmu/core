#!/bin/bash
multisync_version="1.1.2"
multisync_description="番剧/视频cid批量导入工具UI"
multi_sync(){
    clear
    echo "---------------------------"
    echo "        批量导入条目"
    echo "---------------------------"
    echo -e "\033[31m1\033[0m--\033[47;30m批量导入番剧条目\033[0m"
    echo -e "\033[31m2\033[0m--\033[47;30m批量导入视频条目\033[0m"
    echo -e "\033[31m其他键\033[0m--\033[47;30m返回\033[0m"
    read -p "请选择:" switch
    case ${switch} in
    1)  cidmuld_fanju
    ;;
    2)  cidmuld_video
    ;;
    *)  main_loop
    ;;
    esac
}