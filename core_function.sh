#!/bin/bash
core_function_version="0.2.2"
core_function_description="哔哩哔哩弹幕下载核心函数"
core_sha1() {
  sha1sum $1 | awk '{print $1}'
}
core_exit() {
  clear
  exit 0
}
new_setting(){
    echo "danmu_folder=danmu" >setting.cfg
    echo "ua=chrome_linux" >>setting.cfg
    echo "danmu_source=smart" >>setting.cfg
    echo "bg=0" >>setting.cfg
    echo "thread=5" >>setting.cfg
    echo "download_mode=1" >>setting.cfg
    echo "max_analysis_thread=3" >>setting.cfg
    echo "max_download_thread=2" >>setting.cfg
    echo "danmu_debug=no" >>setting.cfg
    echo "res=1920x1080" >>setting.cfg
    echo "tar_enable=\"true\"" >>setting.cfg
    echo "tar_folder=tar_backup" >>setting.cfg
    echo "tar_compress_method=none" >>setting.cfg
    if [ ! -d danmu ]
    then
        mkdir danmu
    fi
    if [ ! -d tar_backup ]
    then
      mkdir tar_backup
    fi
}
determine_jq_and_python(){
  clear
  if ! jq -V >/dev/null
  then
    echo '!!!!!!!!!!!!!!!!!!!!!!!!'
    echo '!   警告：未检测到jq包。  !'
    echo '! 程序大部分功能将会出错。 !'
    echo '!!!!!!!!!!!!!!!!!!!!!!!!'
  fi
  if ! python -V >/dev/null
  then
    echo '!!!!!!!!!!!!!!!!!!!!!!!!'
    echo '!  警告：未检测到python。 !'
    echo '!  弹幕导出将会不可用。    !'
    echo '!!!!!!!!!!!!!!!!!!!!!!!!'
    mv danmaku2ass.py danmaku2ass.py.diabled
  fi
}