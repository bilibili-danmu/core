#!/bin/bash
videocid_version="0.3.3"
videocid_description="视频cid批量获取工具"
cidmuld_video(){
    clear
    echo "---------------------------"
    echo "     批量导入视频条目"
    echo "---------------------------"
    read -p "请输入分区代码:" ps
    real_url="https://api.bilibili.com/archive_rank/getarchiverankbypartion?jsonp=jsonp&tid=${ps}&pn=1"
    curl ${real_url} -H "Referer: https://m.bilibili.com/channel/22.html" -H "${ua_true}" --compressed -s -o getcid.tmp
    cat getcid.tmp|jq '.data.archives[0].tname'>gettname.result.tmp
    cat getcid.tmp|jq '.data.page.count'>count.result.tmp
    rm getcid.tmp
    tname=$(cat gettname.result.tmp)
    count=$(cat count.result.tmp)
    rm gettname.result.tmp
    rm count.result.tmp
    echo "-${ps}"
    echo "|-分区名称 ${tname}"
    echo "|-总数量 ${count}个视频"
    max_page=5
    unset count
    echo "|-页数 ${max_page}"
    current_page=1
    thread=0
    while (( ${current_page} <= ${max_page} ))
    do 
        if [ ${thread} == ${max_analysis_thread} ]
        then
            wait
            thread=0
        fi
        thread=`expr ${thread} + 1`
        cidmuld_video_optimize ${current_page} ${ps} ${thread} ${max_download_thread} &
        current_page=`expr ${current_page} + 1`
    done
    wait
    unset thread
    unset page_done
    unset current_page
    if [ ! -f ${ps}_${tname}_cid.list ]
    then
        touch ${ps}_${tname}_cid.list
    fi
    done_thread=1
    while(( ${done_thread} <= ${max_analysis_thread} ))
    do
        if [ -f cid_${done_thread}.list ]
        then
            cat cid_${done_thread}.list >>${ps}_${tname}_cid.list
            rm cid_${done_thread}.list
        fi
        done_thread=`expr ${done_thread} + 1`
    done
    unset page_done
    unset done_thread
    echo "|-完成。已经将所有条目保存至\"${ps}_${tname}_cid.list\"，是否与默认清单(\"cid.list\")合并?(Y/n)"
    read choice
    case ${choice} in
    n)  
        unset tname
        main_loop
    ;;
    N)  
        unset tname
        main_loop
    ;;
    esac
    cat ${ps}_${tname}_cid.list >>cid.list
    rm ${ps}_${tname}_cid.list
    unset tname
}