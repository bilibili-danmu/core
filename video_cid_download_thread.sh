#!/bin/bash
#需要干的事情:对cid.list兼容性进行检测
video_cid_download_thread_version="0.3.2"
video_cid_download_thread_description="普通视频批量基础线程组件"
cidmuld_video_basic(){
    download_thread=$1
    line=$2
    thread_code=$3
    title="${4}"
    url="https://api.bilibili.com/x/player/pagelist?aid=${line}&jsonp=jsonp"
    curl ${url}  -H 'Origin: https://www.bilibili.com' -H 'Accept: */*' -H 'Referer: https://www.bilibili.com/video/av39674794/?redirectFrom=h5' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36' --compressed -s -o getpcid_${thread_code}_${download_thread}.tmp
    cid_count=0
    cat getpcid_${thread_code}_${download_thread}.tmp|jq '.data[] | .part, .cid'>getpcid_${thread_code}_${download_thread}.result.tmp
    rm getpcid_${thread_code}_${download_thread}.tmp
    echo "  |-线程 ${thread} ${title}"
    echo "-视频${line}" >>cid_${thread_code}_${download_thread}.list
    echo " |-标题:${title}" >>cid_${thread_code}_${download_thread}.list
    echo " |-分集" >>cid_${thread_code}_${download_thread}.list
    cid_count=0
    cat getpcid_${thread_code}_${download_thread}.result.tmp | while read line_2
    do
        if [ ${line_2:0:1} != "\"" ]
        then
            cid_count=`expr ${cid_count} + 1`
            echo "   |-${line_2}" >> cid_${thread_code}_${download_thread}.list
        else
            echo "  |-标题:${line_2}" >> cid_${thread_code}_${download_thread}.list
        fi
    done
    rm getpcid_${thread_code}_${download_thread}.result.tmp
    unset cid_count
}