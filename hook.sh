#!/bin/bash
hook_version="1.0"
hook_description="bash debug钩子"
debug_init(){
    if [ ! -f debug.log ]
    then
        touch debug.log
    fi
    which_init
}
which_init(){
    echo_path=$(which echo)
    rm_path=$(which rm)
    curl_path=$(which curl)
}
echo(){
    DATE=$(date +%H:%M:%S)
    if [ "${echo_path}" == "" ]
    then
        printf "${DATE} 警告:echo指令在未初始化时就被调用。将会调用printf函数。" >>debug.log
        printf "${1}"
    else
        ${echo_path} ${1} ${2} ${3} ${4}
    fi
}
rm(){
    DATE=$(date +%H:%M:%S)
    ${echo_path} "${DATE} 程序尝试调用命令 rm ${1} ${2} ${3} ${4}."
    read -p "是否同意?(Y/n)" agree
    if [ ! "${agree}" == "n" ]
    then
        echo "${DATE} 程序删除了${1} ${2} ${3} ${4}." >>debug.log
        ${rm_path} ${1} ${2} ${3} ${4}
    else
        echo "${DATE} 程序尝试删除${1} ${2} ${3} ${4}，但是已被阻止。" >>debug.log
    fi
}
curl(){
    DATE=$(date +%H:%M:%S)
    echo "${DATE} 程序调用了curl。" >>debug.log
    ${curl_path} ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10} ${11}
}