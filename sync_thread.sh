#!/bin/bash
sync_thread_version="1.2"
sync_thread_description="弹幕下载简单线程组件"
danmu_sync_thread(){
    danmu_source=$1
    thread=$2
    line=$3
    danmu_folder=$4
    last_title=$5
    #不知道为啥有时候会剩下一个引号，最好去除一下
    #7/5:人类的本质是复读机，复读三遍防止出错
    last_title=${last_title/\"/}
    last_title=${last_title/\"/}
    last_title=${last_title/\"/}
    case $danmu_source in
    smart)
        url1="https://api.bilibili.com/x/v1/dm/list.so?oid=${line}"
        url2="http://comment.bilibili.cn/${line}.xml"
        curl ${url1} -H "${ua_true}" --tcp-fastopen --retry 1 --max-time 5 --speed-time 5 --compressed -s -o danmu1_${thread}.tmp
        curl ${url2} -H "${ua_true}" --tcp-fastopen --retry 1 --max-time 5 --speed-time 5 --compressed -s -o danmu2_${thread}.tmp
        if [ -f danmu1_${thread}.tmp ]
        then
            if [ -f danmu2_${thread}.tmp ]
            then
                diff danmu1_${thread}.tmp danmu2_${thread}.tmp > /dev/null
                if [ $? != 0 ]
                then
                a=`du -s danmu1_${thread}.tmp| awk '{print $1}'`
                b=`du -s danmu2_${thread}.tmp| awk '{print $1}'`
                    if [ $a -gt $b ]
                    then
                        cp danmu1_${thread}.tmp ${danmu_folder}/${last_title}_${line}.danmu
                    else
                        cp danmu2_${thread}.tmp ${danmu_folder}/${last_title}_${line}.danmu
                    fi
                else
                    cp danmu1_${thread}.tmp ${danmu_folder}/${last_title}_${line}.danmu
                fi
                rm danmu1_${thread}.tmp
                rm danmu2_${thread}.tmp
            else
                cp danmu1_${thread}.tmp ${danmu_folder}/${last_title}_${line}.danmu
                rm danmu1_${thread}.tmp
            fi
        else
            if [ -f danmu2_${thread}.tmp ]
            then
                cp danmu2_${thread}.tmp ${danmu_folder}/${last_title}_${line}.danmu
                rm danmu2_${thread}.tmp
            else
                echo " |-${thread} 无法获取${line}的弹幕数据。"
            fi
        fi
        unset a
        unset b
        ;;
    api)
        url1="https://api.bilibili.com/x/v1/dm/list.so?oid=${line}"
        curl ${url1} -H "${ua_true}" --tcp-fastopen --retry 1 --max-time 5 --speed-time 5 --compressed -s -o ${danmu_folder}/${last_title}_${line}.danmu
        ;;
    commit)
        url2="http://comment.bilibili.cn/${line}.xml"
        curl ${url2} -H "${ua_true}"  --tcp-fastopen --retry 1 --max-time 5 --speed-time 5 --compressed -s -o ${danmu_folder}/${last_title}_${line}.danmu
        ;;
    esac
}