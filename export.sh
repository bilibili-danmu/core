#!/bin/bash
export_version="0.2.2"
if [ -f danmaku2ass.py ]
then
    export_description="弹幕转换导出组件"
else
    export_description="弹幕转换导出组件(未激活)"
fi
export_danmu_ui(){
    clear
    echo "---------------------------"
    echo "         弹幕导出"
    echo "---------------------------"
    echo -e "\033[31m警告:在处理大量弹幕时可能需要花费大量时间(10秒到1天不等)。\033[0m您可以在后台运行此脚本。"
    echo "输入Y继续，其他键中止。"
    read -p "请选择:" switch
    case ${switch} in
    Y)  auto_export_danmu
    ;;
    *)  main_loop
    ;;
    esac
}
export_danmu(){
    source_file=${1}
    ./danmaku2ass.py -s ${res} -f Bilibili -o danmu_out/${source_file}.ass ${danmu_folder}/${source_file}
    unset source_file
}
auto_export_danmu(){
    echo "正在将所有弹幕导出..."
    if [ ! -d danmu_out ]
    then
        mkdir danmu_out
    fi
    for file in $(ls ${danmu_folder})
    do
        echo "$(date): 正在导出${file}..."
        export_danmu ${file}
    done
    export_danmu_ui
}