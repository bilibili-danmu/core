#!/bin/bash
fanjucid_version="0.3.2"
fanjucid_description="番剧cid批量导入组件"
cidmuld_fanju(){
    clear
    echo "---------------------------"
    echo "         番剧批量导入"
    echo "---------------------------"
    echo "正在获取页码数量..."
    real_url="https://bangumi.bilibili.com/media/web_api/search/result?season_version=-1&area=-1&is_finish=-1&copyright=-1&season_status=-1&season_month=-1&pub_date=-1&style_id=-1&order=3&st=1&sort=0&page=1&season_type=1&pagesize=20"
    curl "${real_url}" -s -S -H "${ua_true}" --compressed -o pagelist.tmp
    unset real_url
    cat pagelist.tmp|jq '.result.page.total'>count.result.tmp
    rm pagelist.tmp
    fan_total=$(cat count.result.tmp)
    rm count.result.tmp
    max_page=`expr ${fan_total} / 20 + 1`
    unset fan_total
    echo "-共${max_page}页"
    for(( page = 1; page <= ${max_page}; page++))
    do
        DATE=$(date +%H:%M:%S)
        echo "|-第${page}页"
        real_url="https://bangumi.bilibili.com/media/web_api/search/result?season_version=-1&area=-1&is_finish=-1&copyright=-1&season_status=-1&season_month=-1&pub_date=-1&style_id=-1&order=3&st=1&sort=0&page=${page}&season_type=1&pagesize=20"
        curl "${real_url}" -s -S -H "${ua_true}" --compressed -o fanlist.tmp
        unset real_url
        cat fanlist.tmp|jq '.result.data[].season_id'>getcid.result.tmp
        rm fanlist.tmp
        for spid in $(cat getcid.result.tmp)
        do
            url="https://bangumi.bilibili.com/web_api/get_ep_list?season_id=${spid}&season_type=1"
            curl "${url}" -s -S -H "${ua_true}" --compressed -o getcid.tmp
            cid_count=0
            cat getcid.tmp|jq '.result[].cid'>getcid.result.tmp
            get_bangumi_title ${spid}
            echo " |-spid=>${spid}"
            echo "  |-${av_title}"
            echo "-番剧${spid}" >>cid.list
            echo " |-标题:${av_title}" >>cid.list
            echo " |-分集"
            for line in $(cat getcid.result.tmp)
            do
                cid_count=`expr ${cid_count} + 1`
                echo "  |-${line}" >> cid.list
            done
            echo "  |-共${cid_count}集"
        done
        rm getcid.result.tmp
    done
}